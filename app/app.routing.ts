import { PyramidComponent } from "./components/pyramid.component";
import { ListComponent } from "./components/list.component";
import { AppComponent } from "./components/app.component";
import { MapComponent } from "./components/map.component";

export const routes = [
  { path: "", component: AppComponent },  
  { path: "list", component: ListComponent },
  { path: "pyramid", component: PyramidComponent },
  { path: "map", component: MapComponent }
];

export const navigatableComponents = [
  AppComponent,
  ListComponent,
  PyramidComponent,
  MapComponent
];