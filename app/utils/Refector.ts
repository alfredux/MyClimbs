export class Reflector{

    theObject: Object;

    constructor(anObject: Object){
        this.theObject = anObject;
    }

    listMethods(){
        //Object.keys(this.theObject).forEach().
        for(var key in this.theObject) {
            console.log('Type: ' + typeof key);
            console.log(key + ' => ' + this.theObject[key]);
        }
    }
}