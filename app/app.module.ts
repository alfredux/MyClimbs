import { NgModule } from "@angular/core";

import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptHttpModule } from "nativescript-angular/http";
import { NativeScriptModule } from "nativescript-angular/platform";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { AppComponent } from "./components/app.component";
import { ListComponent } from "./components/list.component";
import { PyramidComponent } from "./components/pyramid.component";
import { MapComponent } from "./components/map.component";
import { DataModel} from "./data/DataModel";

import { routes, navigatableComponents } from "./app.routing";

export function onMapReady(){
    console.log("RABOOOO!!");
}

@NgModule({
  imports: [
    NativeScriptModule,
    NativeScriptFormsModule,
    NativeScriptHttpModule,
    NativeScriptRouterModule,
    NativeScriptRouterModule.forRoot(routes)
  ],
  declarations: [
    AppComponent,
    ...navigatableComponents
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
