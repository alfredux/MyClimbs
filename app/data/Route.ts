import { DataModel } from "./DataModel";

export class Route  {
    
    name: string;
    grade: string;
    date: string;
    crag: string;
    sector: string;
    style: string;
    
    constructor(name:string, grade: string, date: string, crag: string, sector: string, style: string) {
        this.name=name;
        this.grade=grade;        
        this.date=date;
        this.crag=crag;
        this.sector=sector;
        this.style=style;
    }

    toString(){
        return "name:"+this.name+", grade:"+this.grade+", crag:"+this.crag;
    }

}