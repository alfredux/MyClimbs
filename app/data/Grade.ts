import { DataModel } from "./DataModel";
import { Route } from "./Route";

export class Grade{

    public grade: String;

    public onsight: number;
    public flash: number;
    public redpoint: number;

    public total: number;

    public onsightPercent: String;
    public flashPercent: String;
    public redpointPercent: String;
    public totalGradePercent: String;

    constructor(grade: String, onsight: number, flash: number, redpoint: number){

        this.grade=grade;
        this.onsight=onsight;
        this.flash=flash;
        this.redpoint=redpoint;
        this.total = this.onsight + this.flash + this.redpoint;        
        this.calculatePercents()
    }

    addRoute(route: Route){
        if(route.style=="OS") this.onsight++;
        else if(route.style=="FL") this.flash++;
        else if(route.style=="RP") this.redpoint++;
        this.total++;
        this.calculatePercents();
    }

    calculatePercents(){
        this.onsightPercent = new String(Math.ceil((this.onsight / this.total)*100));
        this.flashPercent = new String(Math.ceil((this.flash / this.total)*100));
        this.redpointPercent = new String(Math.ceil((this.redpoint / this.total)*100));
        this.totalGradePercent = new String(Math.ceil((this.total / DataModel.totalRoutes)*100));
        if(this.onsightPercent!="0") this.onsightPercent+="%";
        if(this.flashPercent!="0") this.flashPercent+="%";
        if(this.redpointPercent!="0") this.redpointPercent+="%"; 
        if(this.totalGradePercent!="0") this.totalGradePercent+="%";
    }

    toString(){
        return this.grade+": "+this.totalGradePercent;
    }
}