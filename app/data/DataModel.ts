import { getFile, getImage, getJSON, getString, request } from "http";
import { XmlParser, ParserEvent } from "xml";
import {Grade} from "./Grade";
import {Route} from "./Route";
import { FileUtil } from "./FileUtil";

export class DataModel{

    public static routes: Array<Route>;    
    public static gradesList: Array<Grade>;
    public static gradesMap: Map<String,Grade>;
    public static totalRoutes: number;

    constructor(){
        console.log("CREATING DATA MODEL");
        
        DataModel.routes = new Array();

        var source = "http://192.168.1.41/selenium/8a.json";

        request({ url: source, method: "GET" })
            .then((response) => {
                if(response.statusCode==200){
                    var data = response.content.toJSON();
                    this.loadDataModel(data);
                    console.log("LOADED "+DataModel.totalRoutes+" ROUTES from "+ source);
                    this.writeDataToJSON(response.content);         
                }else{
                    console.log("Bad status code "+response.statusCode);
                    this.loadDataFromJSON();
                    console.log("LOADED "+DataModel.totalRoutes+" ROUTES from local Database");
                }                                
            }, (e) => {
                console.log("Error in request: " + e);
                var data = require("./DataBase");
                this.loadDataFromJSON();
                console.log("LOADED "+DataModel.totalRoutes+" ROUTES from local Database");
            });
    }

    public writeDataToJSON(data){
        FileUtil.writeJSON("data/8a.json",data)
            .then((response) => {
                this.loadDataModel(response);
            })
            .catch((err) => {
                console.log(err);
            });    
    }

    public loadDataFromJSON(){
        FileUtil.readJSON("data/8a.json")
            .then((response) => {
                this.loadDataModel(response);
            })
            .catch((err) => {
                console.log(err);
            });    
    }   

    public loadDataModel(data){
        for(let line of data){
            var r = new Route(line["Via"], line["Grado"],  line["Fecha"], line["Escuela"], "", line["Estilo"]);
            DataModel.routes.push(r);
        }
               
        DataModel.totalRoutes = DataModel.routes.length;             
        this.initGrades();
    }


    public getRoutes(){
        return DataModel.routes;
    }

    public getGrades(){
        return DataModel.gradesList;
    }

    initGrades(){
        DataModel.gradesMap = new Map<String,Grade>();             
        DataModel.gradesList = new Array<Grade>();

        for(let r of DataModel.routes){
            let gr = DataModel.gradesMap.get(r.grade);
            if(gr==null) {
                gr = new Grade(r.grade,0,0,0);
                DataModel.gradesMap.set(r.grade,gr);
                DataModel.gradesList.push(gr);
            }
            gr.addRoute(r);
        }

    }

}

