import { Component } from "@angular/core";
import { DataModel } from "../data/DataModel";

@Component({
  selector: "pyramid",
  templateUrl: "components/templates/pyramid.component.html",
  styleUrls: ["components/styles/components.css"]
})

export class PyramidComponent {
  
  public theData: DataModel;

  constructor() {
    this.theData = new DataModel();    
  }

}
