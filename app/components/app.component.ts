import { Component } from "@angular/core";
import { Router } from "@angular/router";

import { ListComponent } from "./list.component";
import { PyramidComponent } from "./pyramid.component";
import { MapComponent } from "./map.component";

import observableModule = require("data/observable");


@Component({
    selector: "app",
    templateUrl: "components/templates/app.component.html",
    styleUrls: ["components/styles/components.css"]
})
export class AppComponent {
    
    constructor(private router: Router) {

    }
    
    onPageLoaded(args: observableModule.EventData) {
            console.log("Page Loaded");
    }


    public onPyramid() {
        console.log("clicked on pyramid");
        this.router.navigate(["/pyramid"]);
    }
    
    public onList() {
        console.log("clicked on list");
        this.router.navigate(["/list"]);
    }

    public onMap() {
        console.log("clicked on map");
        this.router.navigate(["/map"]);
    }
}