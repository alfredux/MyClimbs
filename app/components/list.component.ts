import { Component } from "@angular/core";
import { DataModel } from "../data/DataModel";
import { Route } from "../data/Route";

@Component({
  selector: "list",
  templateUrl: "components/templates/list.component.html",
  styleUrls: ["components/styles/components.css"]
})

export class ListComponent {

  public theData: DataModel;

  constructor() {
    this.theData = new DataModel();
  }
  
}
