import { Component } from "@angular/core";
import { DataModel } from "../data/DataModel";
import { TnsGoogleMaps } from "nativescript-googlemaps";
import {Page, NavigatedData} from "ui/page";
import {Frame,AndroidFrame} from "ui/frame";
import {ContentView} from "ui/content-view";

@Component({
  selector: "map",
  templateUrl: "components/templates/map.component.html",
  styleUrls: ["components/styles/components.css"],
  providers: [TnsGoogleMaps]
})


export class MapComponent {

  public theMap: TnsGoogleMaps;
  public theData: DataModel;
  public theFrame: AndroidFrame;

  constructor(private page: Page, private map: TnsGoogleMaps) {

  }
  
  onNavigatedTo(args: NavigatedData) {
    console.log("RABO");
    (<Page>args.object).bindingContext = args.context;
  }

  onMapReadyAlfredo(args: NavigatedData){
    console.log("WTF");
  }
  

}
